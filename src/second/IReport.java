package second;

import java.util.List;

public interface IReport {
    public void output(List<ReportItem> items);
}
